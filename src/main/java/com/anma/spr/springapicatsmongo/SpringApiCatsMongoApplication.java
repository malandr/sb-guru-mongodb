package com.anma.spr.springapicatsmongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringApiCatsMongoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringApiCatsMongoApplication.class, args);
	}

}
