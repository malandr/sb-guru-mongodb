package com.anma.spr.springapicatsmongo.services;

import com.anma.spr.springapicatsmongo.models.Cat;
import com.anma.spr.springapicatsmongo.repositories.CatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CatServiceImpl implements CatService {

    private final CatRepository catRepository;

    @Autowired
    public CatServiceImpl(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @Override
    public Cat findCatByColor(String color) {

        Cat cat = null;

        for (Cat catTemp : catRepository.findAll()) {
            if (catTemp.getColor().equals(color)) cat = catTemp;
        }

        return cat;
    }

//    @Override
//    public Cat findCatById(UUID id) {
//
//        Cat cat = null;
//
//        for (Cat catTemp : catRepository.findAll()) {
//            if (catTemp.getId() == id) cat = catTemp;
//        }
//
//        return cat;
//    }

    @Override
    public Cat createCat(Cat cat) {

        Cat newCat = new Cat();

        newCat.setId(UUID.randomUUID());
        newCat.setName(cat.getName());
        newCat.setAge(cat.getAge());
        newCat.setColor(cat.getColor());

        catRepository.save(newCat);

        return newCat;
    }
}
