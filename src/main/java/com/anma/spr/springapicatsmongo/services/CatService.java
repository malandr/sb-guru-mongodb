package com.anma.spr.springapicatsmongo.services;

import com.anma.spr.springapicatsmongo.models.Cat;
import org.springframework.stereotype.Service;

import java.util.UUID;

public interface CatService {

    Cat findCatByColor(String color);
//    Cat findCatById(UUID id);
    Cat createCat(Cat cat);
}
