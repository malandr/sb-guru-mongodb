package com.anma.spr.springapicatsmongo;

import com.anma.spr.springapicatsmongo.models.Cat;
import com.anma.spr.springapicatsmongo.repositories.CatRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class Bootstrap implements CommandLineRunner {

    private final CatRepository catRepository;

    public Bootstrap(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        loadData();
    }

    private void loadData() {

        if (catRepository.count() == 0) {

            Cat cat = new Cat();
            cat.setId(UUID.randomUUID());
            cat.setName("Vasyl");
            cat.setAge(10);
            cat.setColor("grey");

            catRepository.save(cat);
        }
    }
}
