package com.anma.spr.springapicatsmongo.repositories;

import com.anma.spr.springapicatsmongo.models.Cat;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public interface CatRepository extends MongoRepository<Cat, UUID> {

}
