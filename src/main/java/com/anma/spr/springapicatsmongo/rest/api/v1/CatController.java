package com.anma.spr.springapicatsmongo.rest.api.v1;

import com.anma.spr.springapicatsmongo.models.Cat;
import com.anma.spr.springapicatsmongo.models.CatListDTO;
import com.anma.spr.springapicatsmongo.repositories.CatRepository;
import com.anma.spr.springapicatsmongo.services.CatService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping(CatController.BASE_URL)
public class CatController {

    static final String BASE_URL = "/api/v1/cats";

    private final CatService catService;
    private final CatRepository catRepository;

    public CatController(CatService catService, CatRepository catRepository) {
        this.catService = catService;
        this.catRepository = catRepository;
    }

    @GetMapping
    public ResponseEntity<CatListDTO> getListOfCats() {

        return new ResponseEntity<CatListDTO>(new CatListDTO(catRepository.findAll()),
                HttpStatus.OK);
    }

    @GetMapping("/{catId}")
    public ResponseEntity<Cat> getCat(@PathVariable("catId") UUID catId) {

        return new ResponseEntity<Cat>(catRepository.findById(catId).get(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Cat> saveCat(@Valid @RequestBody Cat cat) {

        Cat savedCat = catService.createCat(cat);

        HttpHeaders httpHeaders = new HttpHeaders();

        httpHeaders.add("Location", savedCat.getId().toString());

        return new ResponseEntity<Cat>(httpHeaders, HttpStatus.CREATED);
    }




}
