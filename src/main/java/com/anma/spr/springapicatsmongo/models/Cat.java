package com.anma.spr.springapicatsmongo.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Data
@Builder
@Document(collection = "cats")
@NoArgsConstructor
@AllArgsConstructor
public class Cat {

    private UUID id;
    private String name;
    private int age;
    private String color;

}
